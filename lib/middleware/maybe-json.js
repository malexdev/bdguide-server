// extend express's `res` function to add support for res.json to return a 404 if passed a null

function maybeJson(req, res, next) {
  res._json = res.json
  res.json = (j) => {
    if (j) {
      res._json(j)
    } else {
      next()
    }
  }
  next()
}

module.exports = maybeJson