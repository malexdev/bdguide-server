const Promise = require('bluebird')

// TODO: Hook this file up to the actual database

// cooldown is how many minutes after a kill the boss is guaranteed to stay despawned
// range is the window in which the boss will spawn
// killed is the timestamp for the last time the boss was killed
// spotted is the timestamp for the last time the boss was reported up
const bosses = [
  { id: 'bheg', name: 'Dastard Bheg', cooldown: 600, range: 240, killed: null, spotted: null },
  { id: 'rednose', name: 'Red Nose', cooldown: 600, range: 240, killed: null, spotted: null }
]

function list() {
  return new Promise((res, rej) => res(bosses))
}

function find(id) {
  return new Promise((res, rej) => res(bosses.find(x => x.id === id)))
}

module.exports = {
  list,
  find
}