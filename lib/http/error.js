const isProduction = process.env.NODE_ENV === 'production'

function notFound(req, res) {
  res.status(404).json({ error: 'Resource does not exist', code: 404 })
}

function internalError(err, req, res, next) {
  if (!err) { return next() }
  res.status(err.status || 500).json({
    message: err.message,
    error: isProduction ? {} : err
  })
}

module.exports = {
  notFound,
  internalError
}