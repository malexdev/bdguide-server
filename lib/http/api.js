const express = require('express')

// import api functions
const ping = require('./ping')
const bosses = require('./bosses')

// create the express api router
const api = express.Router()

// add api methods
api.get('/ping', ping)
api.get('/bosses', bosses.list)
api.get('/bosses/:id', bosses.find)

// alright, export it
module.exports = api