const db = require('../data/bosses')

function list(req, res, next) {
  return db.list().then(bosses => res.json(bosses)).catch(err => next(err))
}

function find(req, res, next) {
  if (!req.params.id) { return next('No Boss ID') }
  return db.find(req.params.id).then(boss => res.json(boss)).catch(err => next(err))
}

module.exports = {
  list,
  find
}