const express = require('express')
const morgan = require('morgan')
const maybeJson = require('./lib/middleware/maybe-json')

const api = require('./lib/http/api')
const ping = require('./lib/http/ping')
const error = require('./lib/http/error')

// set up express
const app = express()

// set up middleware
app.use(morgan(':method :url (:status) - :res[content-length] bytes - :response-time ms'))
app.use(maybeJson)
app.get('/', ping) 
app.use('/api', api)
app.use(error.internalError)
app.use(error.notFound)

// we're ready to go!
app.listen(3000, () => console.log('Server started'))